#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OUTDIR=${OUTDIR:-$SCRIPTDIR/run}
SLEEP=${SLEEP:-0}
ONLY=${ONLY:-}
UNSERVED_SRC=${UNSERVED_SRC:-$SCRIPTDIR/unserved-testbench}
UNSERVED_JAR=${UNSERVED_JAR:-$UNSERVED_SRC/target/unserved-testbench-1.0-SNAPSHOT.jar}
COMPOSIT_SRC=${COMPOSIT_SRC:-$SCRIPTDIR/composit}
COMPOSIT_JAR=${COMPOSIT_JAR:-$COMPOSIT_SRC/composit-cli/target/composit-cli-0.1.0-SNAPSHOT-full.jar}

WSC_CONVERTED="${SCRIPTDIR}/wsc-converted"
WSC_ORIGINAL="${SCRIPTDIR}/sws-test-collections/src/main/resources/services/wsc08/"
CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscRawExperimentRunner
PP_CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.pragproof_design.PPRawExperimentDesignRunner
WSCCONVERTER_CLASSNAME=br.ufsc.lapesd.unserved.testbench.benchmarks.wsc08.WscProblemConverterApp

if [ -z "${ONLY}" -o -n "$(echo "${ONLY}" | grep composit-pilot)" ]; then RUN_COMPOSIT_PILOT=1; fi
if [ -z "${ONLY}" -o -n "$(echo "${ONLY}" | grep composit-long)" ]; then RUN_COMPOSIT_LONG=1; fi
if [ -z "${ONLY}" -o -n "$(echo "${ONLY}" | grep wsc-pilot)" ]; then RUN_WSC_PILOT=1; fi
if [ -z "${ONLY}" -o -n "$(echo "${ONLY}" | grep wsc-long)" ]; then RUN_WSC_LONG=1; fi
if [ -z "${ONLY}" -o -n "$(echo "${ONLY}" | grep pp)" ]; then RUN_PP=1; fi
if [ -n "${RUN_COMPOSIT_PILOT}" -o -n "${RUN_COMPOSIT_LONG}" ]; then RUN_COMPOSIT=1; fi
if [ -n "${RUN_WSC_PILOT}" -o -n "${RUN_WSC_LONG}" ]; then RUN_WSC=1; fi

if [ ! -z "$1" ]; then
  OUTDIR=$1
fi

if [ ! -d "${OUTDIR}" ]; then
  mkdir -p $OUTDIR
fi

if [ ! -d "${OUTDIR}" ]; then
  echo "${OUTDIR} is not a directory!" 
  exit 1
fi
OUTDIR=$(realpath "${OUTDIR}")

# --- fix a cpu clock speed
set-cpufreq.sh && CLOCKSET=1 || echo "Failed to set a cpu clock speed, ignoring"

# --- download unserved-testbench, if needed
if [ ! -d "${UNSERVED_SRC}" ]; then
    pushd "${SCRIPTDIR}"
    git clone --depth 1 https://bitbucket.org/alexishuf/unserved-testbench.git || exit 1
    pushd unserved-testbench
    git config submodule."src/main/resources/unserved".url http://bitbucket.org/alexishuf/unserved.git
    git submodule update --init --recursive
    popd
    UNSERVED_SRC="${SCRIPTDIR}/unserved-testbench"
    UNSERVED_JAR="${UNSERVED_SRC}/target/unserved-testbench-1.0-SNAPSHOT.jar"
    popd
fi

# --- download composit, if needed
if [ ! -d "${COMPOSIT_SRC}" ]; then
    pushd "${SCRIPTDIR}"
    git clone --depth 1 https://github.com/citiususc/composit.git || exit 1
    COMPOSIT_SRC="${SCRIPTDIR}/composit"
    COMPOSIT_JAR="${COMPOSIT_SRC}/composit-cli/target/composit-cli-0.1.0-SNAPSHOT-full.jar"
    pushd "${COMPOSIT_SRC}"
    git apply "${SCRIPTDIR}/composit-instrumentation.diff" || exit 1
    popd
    popd
fi

# --- package unserved-testbench
if [ -z "${SKIPBUILD}" -a -n "${UNSERVED_SRC}" -a -n "${RUN_WSC}" ]; then
  pushd "${UNSERVED_SRC}"
  mvn package || exit 1
  popd
fi

# --- package composit
if [ -z "${SKIPBUILD}" -a -n "${COMPOSIT_SRC}" -a -n "${RUN_COMPOSIT}" ]; then
  pushd "${COMPOSIT_SRC}"
  mvn package assembly:single || exit 1
  popd
fi

# --- convert WSC'08 problems, if needed
if [ ! -d "${WSC_CONVERTED}" ]; then
    if [ ! -d "${WSC_ORIGINAL}" ]; then
	echo "WSC'08 files not found at ${WSC_ORIGINAL}, downloading..."
	pushd "${SCRIPTDIR}"
	git clone --depth 1 "https://github.com/kmi/sws-test-collections.git" || exit 1
	WSC_ORIGINAL="${SCRIPTDIR}/sws-test-collections/src/main/resources/services/wsc08"
	popd
    fi

    echo "uNSERVED WSC'08 files not found at ${WSC_CONVERTED}, converting..."
    mkdir -p "${WSC_CONVERTED}" || exit 1
    java -cp "${UNSERVED_JAR}" $WSCCONVERTER_CLASSNAME --wsc-root "${WSC_ORIGINAL}" \
	 --out-root "${WSC_CONVERTED}" \
         --nfp-assigner-cfg "${SCRIPTDIR}/nfpAssigner.config.json"
    if [ ! $? -eq 0 ]; then
	echo "Failed to convert WSC'08 problems"; exit 1
    fi
fi

# --- write commits
if [ -n "${RUN_COMPOSIT}" ]; then
  pushd "${COMPOSIT_SRC}"
  git log -1 > "${OUTDIR}/composit.commit"
  popd
fi
if [ -n "${RUN_WSC}" ]; then
  pushd "${UNSERVED_SRC}"
  git log -1 > "${OUTDIR}/unserved-testbench.commit"
  popd
fi

# --- run tests

if [ -f "${COMPOSIT_JAR}" ]; then
  if [ -n "${RUN_COMPOSIT_PILOT}" ]; then
    echo "problem,graphConstruction,graphOptimization,composition,graphConstructionMem,graphOptimizationMem,compositionMem,maxMem" >  "${SCRIPTDIR}/results-composit-pilot.csv"
    for rep in {1..9}; do
      for problem in $(seq 8 | shuf); do
        echo -e "\n\n rep=${rep} problem=${problem}"
        java -Xms2048m -Xmx2048m -jar "${COMPOSIT_JAR}" compose -d TESTSET_2008_0${problem} 2>&1 \
          | tee logs/composit-pilot.run.${i}.${j}.log
        cat /tmp/composit-times >> "${SCRIPTDIR}/results-composit-pilot.csv"
        sleep $(echo "scale=3; ${SLEEP}/1000")s
      done
    done
  fi

  if [ -n "${RUN_COMPOSIT_LONG}" ]; then
    echo "problem,graphConstruction,graphOptimization,composition,graphConstructionMem,graphOptimizationMem,compositionMem,maxMem" >  "${SCRIPTDIR}/results-composit-long.csv"
    for rep in {1..35}; do
      for problem in $(seq 8 | shuf); do
        echo -e "\n\n rep=${rep} problem=${problem}"
        java -Xms2048m -Xmx2048m -jar "${COMPOSIT_JAR}" compose -d TESTSET_2008_0${problem} 2>&1 \
          | tee logs/composit-long.run.${i}.${j}.log
        cat /tmp/composit-times >> "${SCRIPTDIR}/results-composit-long.csv"
        sleep $(echo "scale=3; ${SLEEP}/1000")s
      done
    done
  fi
fi

if [ -n "${RUN_WSC_PILOT}" ]; then
  java -cp "${UNSERVED_JAR}" $CLASSNAME --design-csv "${SCRIPTDIR}/wsc-pilot.csv" \
      --results-csv "${OUTDIR}/results-wsc-pilot.csv" \
      --result-objects-dir "${OUTDIR}/results-wsc-pilot" --continue "${OUTDIR}/state-wsc-pilot.json" 2>&1 \
      --sleep ${SLEEP} \
    | tee -a "${OUTDIR}/log-wsc-pilot"
  cp "${OUTDIR}/results-wsc-pilot.csv" "${SCRIPTDIR}"
fi

if [ -n "${RUN_WSC_LONG}" ]; then
  java -cp "${UNSERVED_JAR}" $CLASSNAME --design-csv "${SCRIPTDIR}/wsc-long.csv" \
      --results-csv "${OUTDIR}/results-wsc-long.csv" \
      --result-objects-dir "${OUTDIR}/results-wsc-long" --continue "${OUTDIR}/state-wsc-long.json" 2>&1 \
      --sleep ${SLEEP} \
    | tee -a "${OUTDIR}/log-wsc-long"
  cp "${OUTDIR}/results-wsc-long.csv" "${SCRIPTDIR}"
fi

if [ -n "${RUN_PP}" ]; then
  java -cp "${UNSERVED_JAR}" ${PP_CLASSNAME} --design-csv "${SCRIPTDIR}/pp.csv" \
      --results-csv "${OUTDIR}/results-pp.csv" \
      --result-objects-dir "${OUTDIR}/results-pp" --continue "${OUTDIR}/state-pp.json" 2>&1 \
      --sleep ${SLEEP} \
    | tee -a "${OUTDIR}/log-pp"
  cp "${OUTDIR}/results-pp.csv" "${SCRIPTDIR}"
fi

# --- restore cpu clock
if [ -n "${CLOCKSET}" ]; then
   unset-cpufreq.sh
fi

# --- process data
#pushd "${SCRIPTDIR}/iscc"
#R --no-save < iscc.R
#popd



