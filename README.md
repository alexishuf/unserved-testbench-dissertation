uNSERVED experiments for the masters dissertation
=================================================

This repository contains experiments data for the dissertation entitled "An Architecture for Composition of Services with Heterogeneous Interaction Models" by Alexis Huf There are two types of things here:

* `run.sh` is a do-it-all script to download & compile code, run the experiments and process the data;
* `results-*.csv`: these files contains the results analyzed in the dissertation. The dissertation provides a fair overview of the analysis process and the R scripts that did the analysis are found in the [dissertation repository][1]



Running
-------

First, check the following requirements:

* At least 600M of free disk space
* Enough free RAM to spawn a JVM with 2048M maximum heap
* JDK >= 1.8
* Maven 3
* R (tested on 3.3.2)
* Git
* A Non-ancient bash
* A Internet connection and a some time
* If you want to set a CPU frequency cap, do it on `set-cpufreq.sh` and `unset-cpufreq.sh` scripts somewhere on your `$PATH` that can run as the user runnrun.sh 

To get started, simply type `./run.sh` and wait (a lot). The following will happen:

* [unserved-testbench][2] will be downloaded (to this directory) compiled, tested and packaged as an über-jar
* [sws-test-collections][3] will be downloaded (WSC'08 XML files come from here)
* [composit][4] will be downloaded, patched (for instrumentation) and packaged
* WSC'08 problems will be converted to uNSERVED and saved under `wsc-converted`, this takes a while, specially number 8
* All experiments will run, which will take *A LOT* of time:
    * `composit-pilot`: Not timed, less than 20m
    * `composit-long`: Not timed, around 1 hour 
    * `wsc-pilot`: 4h39m
    * `wsc-long`: 11h38m
    * `pp`: 2h47m

Usually you will want to run a specific set of experiments, avoid building unserved-testbench (because it is already built) and add a sleep in between runs to avoid burning your machine (if you have no A/C). Do it like this:

```
ONLY='composit-pilot wsc-pilot' SKIPBUILD=1 SLEEP=2000 ./run.sh
```


[1]: https://bitbucket.org/alexishuf/dissertacao
[2]: https://bitbucket.org/alexishuf/unserved-testbench
[3]: https://github.com/kmi/sws-test-collections
[4]: https://github.com/citiususc/composit
